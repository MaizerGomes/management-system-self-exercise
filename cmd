#!/usr/bin/env php
<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 9:51 AM
 */

use eKuSchoolIn\providers\CMDServiceProvider;

require_once __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);
session_start();
ob_start();
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$cmd = new CMDServiceProvider($argv);
$cmd->handle();