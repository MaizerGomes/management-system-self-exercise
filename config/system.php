<?php
/**
 * Criado por Maizer Aly de O. Gomes para sigeup.
 * Email: maizer.gomes@gmail.com / maizer.gomes@outlook.com
 * Usuário: Maizer
 * Data: 29/04/2015
 * Hora: 23:50
 */
return [
    'locale'   => 'en',
    'timezone' => 'UTC',
    'debug'    => getenv('APP_DEBUG'),
];
