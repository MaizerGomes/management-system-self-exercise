<?php

use eKuSchoolIn\interfaces\DBMigrationInterface;
use eKuSchoolIn\providers\DBServiceProvider;
use eKuSchoolIn\traits\MigrationRunner;

/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 4:37 PM
 */
class CreateUsersTableMigration extends ConnectionProvider implements DBMigrationInterface
{
    use MigrationRunner;

    public static function sqlQuery()
    {
        $sql = "CREATE TABLE users (id serial)";

        return $sql;
    }

    public static function rollbackQuery()
    {
        return "DROP TABLE IF EXISTS users";
    }
}