<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 4:31 PM
 */

class DBMigrator
{
    public function run($rollback = false)
    {
        $this->runMigration(CreateUsersTableMigration::class, $rollback);
    }

    public function runMigration($className, $rollback = false)
    {
        call_user_func([$className, ($rollback) ? 'rollback' : 'run']);
    }
}