<?php

use eKuSchoolIn\providers\DBServiceProvider;

/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/5/20
 * Hora: 10:35 AM
 */
abstract class ConnectionProvider
{
    /**
     * @var \eKuSchoolIn\providers\DBServiceProvider|\PDO
     */
    protected $db;

    public function __construct()
    {
        $this->db = DBServiceProvider::getInstance();
    }
}