<?php

use eKuSchoolIn\providers\AppServiceProvider;

require_once __DIR__ . '/../init.php';

$app = new AppServiceProvider();

$app->response()->send();
$app->terminate();