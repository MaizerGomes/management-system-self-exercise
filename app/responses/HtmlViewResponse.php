<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 12:37 PM
 */

namespace eKuSchoolIn\responses;


use eKuSchoolIn\interfaces\ControllerResponseInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class HtmlViewResponse implements ControllerResponseInterface
{

    static    $twigModel;
    static    $twigLoader;
    protected $view;
    /**
     * @var array
     */
    protected $data;
    /**
     * @var bool
     */
    protected $render;

    public function __construct($view, $data = [], $render = false)
    {
        $this->view = $view;
        $this->data = $data;
        $this->render = $render;
    }

    /**
     * @return void|string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render()
    {
        $view = str_replace('.', '/', $this->view);
        $file = $view . '.html.twig';

        self::$twigLoader = new FilesystemLoader(__DIR__ . '/../views');
        self::$twigModel = new Environment(self::$twigLoader, [
            'cache' => (getenv('APP_ENV') == 'local') ? false : __DIR__ . '/../views/cache',
        ]);

//        self::setAppFunctions();

        if ($this->render) {
            return self::$twigModel->render($file, $this->data);
        }

        echo self::$twigModel->render($file, $this->data);;
    }
}