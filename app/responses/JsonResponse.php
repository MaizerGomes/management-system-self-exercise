<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 12:37 PM
 */

namespace eKuSchoolIn\responses;


use eKuSchoolIn\interfaces\ControllerResponseInterface;

class JsonResponse implements ControllerResponseInterface
{
    /**
     * @var false|string
     */
    protected $json;

    public function __construct($data = [])
    {
        $this->json = json_encode($data);
    }

    public function render()
    {
        echo $this->json;
    }
}