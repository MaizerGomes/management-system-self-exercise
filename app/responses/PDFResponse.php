<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 12:37 PM
 */

namespace eKuSchoolIn\responses;


use eKuSchoolIn\interfaces\ControllerResponseInterface;
use Spipu\Html2Pdf\Html2Pdf;

class PDFResponse implements ControllerResponseInterface
{
    /**
     * @var \eKuSchoolIn\responses\HtmlViewResponse
     */
    protected $htmlRenderizer;
    private   $view;
    /**
     * @var array
     */
    private $data;
    /**
     * @var string
     */
    private $orientation;
    /**
     * @var string
     */
    private $format;

    public function __construct($view, $data = [], $orientation = 'P', $format = 'A4')
    {
        $this->htmlRenderizer = new HtmlViewResponse($view, $data, true);
        $this->orientation = $orientation;
        $this->format = $format;
    }

    /**
     * @return string
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function render()
    {
        $html2pdf = new HTML2PDF($this->orientation, $this->format, 'pt', true, 'UTF-8', [15, 5, 15, 15]);
        $html2pdf->WriteHTML($this->htmlRenderizer->render());

        return $html2pdf->Output();
    }
}