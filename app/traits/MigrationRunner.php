<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/5/20
 * Hora: 10:50 AM
 */

namespace eKuSchoolIn\traits;


trait MigrationRunner
{
    public static function run()
    {
        $self = new self();

        $self->db->query(self::sqlQuery());
    }

    public static function rollback()
    {
        $self = new self();

        $self->db->query(self::rollbackQuery());
    }
}