<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 10:43 AM
 */

namespace eKuSchoolIn\middlwares;


use eKuSchoolIn\interfaces\MiddlewareInterface;

abstract class BaseMiddleware implements MiddlewareInterface
{

}