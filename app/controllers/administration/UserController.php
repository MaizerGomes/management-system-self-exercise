<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 3:37 PM
 */

namespace eKuSchoolIn\controllers\administration;


use eKuSchoolIn\controllers\BaseController;
use eKuSchoolIn\responses\HtmlViewResponse;
use eKuSchoolIn\responses\JsonResponse;
use eKuSchoolIn\responses\PDFResponse;

class UserController extends BaseController
{
    public function index()
    {
        return new JsonResponse(['names' => 'FFads']);
    }

}