<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 12:33 PM
 */

namespace eKuSchoolIn\controllers;


use eKuSchoolIn\interfaces\RequestServiceProviderInterface;
use eKuSchoolIn\responses\HtmlViewResponse;

class DashboardController extends BaseController
{

    /**
     * @param \eKuSchoolIn\interfaces\RequestServiceProviderInterface $request
     *
     * @return \eKuSchoolIn\responses\HtmlViewResponse
     */
    public function index(RequestServiceProviderInterface $request)
    {
        return new HtmlViewResponse('dashboard');
    }
}