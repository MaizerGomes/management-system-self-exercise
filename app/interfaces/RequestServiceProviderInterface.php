<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 11:07 AM
 */

namespace eKuSchoolIn\interfaces;


interface RequestServiceProviderInterface
{
    public function files($name = null);

    public function hasFiles(): bool;

    public function isGet(): bool;

    public function isPost(): bool;

    public function isType($name): bool;

    public function router(): RouterInterface;

    public function getController(): string;

    public function getMethod(): string;
}