<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 12:11 PM
 */

namespace eKuSchoolIn\interfaces;


interface RouterInterface
{
    public function __construct(RequestServiceProviderInterface $requestServiceProvider);

    public function parseAction(): array;

    public function getControllerFQDN(): string;

    public function getMethodName(): string;

    public function getId();
}