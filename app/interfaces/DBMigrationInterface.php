<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 4:37 PM
 */

namespace eKuSchoolIn\interfaces;


interface DBMigrationInterface
{
    public static function run();
    public static function rollback();
}