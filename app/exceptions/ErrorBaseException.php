<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/3/20
 * Hora: 10:27 AM
 */

namespace eKuSchoolIn\exceptions;


class ErrorBaseException extends \Exception
{

}