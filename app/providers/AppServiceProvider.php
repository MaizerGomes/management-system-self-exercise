<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/3/20
 * Hora: 10:08 AM
 */

namespace eKuSchoolIn\providers;


use eKuSchoolIn\interfaces\RequestServiceProviderInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @var \eKuSchoolIn\interfaces\ControllerResponseInterface
     */
    protected $output;

    /**
     * AppServiceProvider constructor.
     *
     */
    public function __construct()
    {
        $this->boot();

        $request = new RequestServiceProvider();

        $this->handle($request);
    }

    private function boot()
    {
    }

    /**
     * @param \eKuSchoolIn\interfaces\RequestServiceProviderInterface $request
     */
    public function handle(RequestServiceProviderInterface $request)
    {
        $this->output = call_user_func($request->router()->parseAction(), $request);
    }

    public function response()
    {
        return new ResponseServiceProvider($this->output);
    }

    public function terminate()
    {
        //TODO: DO a set of actions after output was sent
        exit();
    }
}