<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/3/20
 * Hora: 12:26 PM
 */

namespace eKuSchoolIn\providers;


use Spipu\Html2Pdf\Html2Pdf;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class ViewServiceProvider extends ServiceProvider
{
    static $twigModel;
    static $twigLoader;

    /**
     * @param        $view
     * @param array  $data
     * @param string $orientation
     * @param string $format
     *
     * @return string
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function pdf($view, $data = [], $orientation = 'P', $format = 'A4')
    {
        $content = static::twig($view, $data, true);

        $html2pdf = new HTML2PDF($orientation, $format, 'pt', true, 'UTF-8', [15, 5, 15, 15]);
        $html2pdf->WriteHTML($content);

        return $html2pdf->Output();
    }

    /**
     * @param       $view
     * @param array $data
     * @param bool  $render
     *
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public static function twig($view, $data = [], $render = false)
    {
        $view = str_replace('.', '/', $view);
        $file = $view . '.html.twig';

        self::$twigLoader = new FilesystemLoader(__DIR__ . '/../views');
        self::$twigModel = new Environment(self::$twigLoader, [
            'cache' => __DIR__ . '/../views/cache',
        ]);

//        self::setAppFunctions();

        if ($render) {
            return self::$twigModel->render($file, $data);
        }

        echo self::$twigModel->render($file, $data);

        exit();
    }
}