<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 4:40 PM
 */

namespace eKuSchoolIn\providers;


use eKuSchoolIn\interfaces\ServiceProviderInterface;
use http\Exception\InvalidArgumentException;

class CMDServiceProvider extends ServiceProvider implements ServiceProviderInterface
{
    /**
     * @var \DBMigrator
     */
    protected $migrator;

    protected $functions = ['migration'];
    /**
     * @var array
     */
    private $args;

    public function __construct($args)
    {
        $this->boot();

        array_shift($args);

        if (!count($args)) {
            throw new \InvalidArgumentException();
        }

        $this->args = $args;

        if (!in_array($this->getFunction(), $this->functions)) {
            throw new \InvalidArgumentException();
        }
    }

    public function boot()
    {
        $this->migrator = new \DBMigrator();
    }

    public function getFunction()
    {
        return (isset($this->args[0])) ? $this->args[0] : null;
    }

    public function handle()
    {
        switch ($this->args[0]) {
            case 'migration':

                $this->checkAvailableOptions(['migrate', 'rollback'], false);

                $this->migrator->run($this->getOption() == 'rollback' ? true : false);
                break;
        }
    }

    public function checkAvailableOptions($options = [], $required = true)
    {
        if (!$required && is_null($this->getOption())) {
            return;
        }

        if (!in_array($this->getOption(), $options)) {
            throw new \InvalidArgumentException();
        }
    }

    public function getOption()
    {
        return (isset($this->args[1])) ? $this->args[1] : null;
    }
}