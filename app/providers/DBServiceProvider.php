<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/3/20
 * Hora: 10:13 AM
 */

namespace eKuSchoolIn\providers;


use PDO;
use PDOException;
use eKuSchoolIn\providers\ConfigServiceProvider as Config;

class DBServiceProvider
{
    /**
     * @var PDO Retorna a instância da conexão a base de dados.
     */
    protected static $instance = null;
    /**
     * @var string
     */
    public    $erroMsg = 'Connection Error!';
    protected $pdo;
    protected $query;
    protected $error   = false;
    protected $results;
    protected $count   = 0;
    protected $lastID  = null;

    public function __construct()
    {
        $default = Config::get('database.default');
        $config = Config::get('database.connections.' . $default);
        $driver = $config['driver'];
        $dbname = $config['database'];
        $host = $config['host'];
        $port = $config['port'];
        $username = $config['username'];
        $password = $config['password'];
        try {
            $this->pdo = new PDO("$driver:dbname=$dbname host=$host port=$port", "$username", "$password", [PDO::ATTR_PERSISTENT => false]);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die($this->erroMsg);
        }
    }

    /**
     * @return \eKuSchoolIn\providers\DBServiceProvider|\PDO
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new DBServiceProvider();
        }

        return self::$instance;
    }

    public function beginTransaction()
    {
        $this->pdo->beginTransaction();

        return $this;
    }

    public function cancel()
    {
        $this->pdo->rollBack();

        return $this;
    }

    public function execute()
    {
        $this->pdo->commit();

        return $this;
    }

    public function lastInsertedID()
    {
        if (!$this->error()) {
            return $this->lastID;
        } else {
            return false;
        }
    }

    public function error()
    {
        return $this->error;
    }

    public function first($collection = false)
    {
        if ($collection) {
            return $this->results(true)->first();
        }

        if ($this->count()) {

            return $this->results()[0];
        }

        return new \stdClass();
    }

    public function results($collection = false)
    {
//        if ($collection) {
//            return collect($this->_results);
//        }

        return $this->results;
    }

    /**
     * Retorna a contagem das linhas do resultado
     *
     * @return bool|PDO
     */
    public function count()
    {
        if (!$this->error()) {
            return $this->count;
        } else {
            return 0;
        }
    }

    /**
     * Função para gerar a query de execução
     *
     * @param string $sql    A query SQL
     * @param array  $params Os parâmetros para fazer bind
     *
     * @return $this
     */
    public function query($sql, $params = [])
    {
        $this->error = false;

        if ($this->query = $this->pdo->prepare($sql)) {
            $x = 1;
            if (count($params)) {
                foreach ($params as $param) {
                    $this->query->bindValue($x, $param);
                    $x++;
                }
            }

            try {
                $this->query->execute();
                $this->results = $this->query->fetchAll(Config::get('database.fetch'));
                $this->count = $this->query->rowCount();
                $this->lastID = null;//$this->pdo->lastInsertId();

            } catch (PDOException $e) {
                $this->error = true;

                if (Config::get('system.debug')) {
                    $this->erroMsg = $e->getMessage();
                    throw $e;
                }
            }
        }

        return $this;
    }
}