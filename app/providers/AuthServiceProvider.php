<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/3/20
 * Hora: 12:57 PM
 */

namespace eKuSchoolIn\providers;


class AuthServiceProvider
{

    public static function isAuthenticated()
    {
        return true;
    }
}