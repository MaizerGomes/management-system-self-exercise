<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/3/20
 * Hora: 10:13 AM
 */

namespace eKuSchoolIn\providers;


use eKuSchoolIn\interfaces\RequestServiceProviderInterface;
use eKuSchoolIn\interfaces\RouterInterface;
use stdClass;

/**
 * Class RequestServiceProvider
 *
 * @package eKuSchoolIn\providers
 */
class RequestServiceProvider implements RequestServiceProviderInterface
{
    /**
     * @var object|\stdClass
     */
    protected $input;
    /**
     * @var object|\stdClass
     */
    protected $files;
    /**
     * @var object|\stdClass
     */
    protected $get;
    /**
     * @var \eKuSchoolIn\providers\RouterServiceProvider
     */
    protected $router;

    /**
     * RequestServiceProvider constructor.
     */
    public function __construct()
    {
        $this->input = (isset($_POST)) ? (object)$_POST : new StdClass();
        $this->files = (isset($_FILES)) ? (object)$_FILES : new StdClass();
        $this->get = (isset($_GET)) ? (object)$_GET : new StdClass();
        $this->router = new RouterServiceProvider($this);
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        if (!$name) {
            return null;
        }

        return (isset($this->input->$name)) ? $this->input->$name : null;
    }

    /**
     * @param null $name
     *
     * @return object|\stdClass|null
     */
    public function files($name = null)
    {
        if (!$name) {
            return $this->files;
        }

        return (isset($this->files->$name)) ? $this->files->$name : null;
    }

    /**
     * @return bool
     */
    public function hasFiles(): bool
    {
        return (boolean)count((array)$this->files);
    }


    /**
     * @return bool
     */
    public function isGet(): bool
    {
        return $this->isType('GET');
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function isType($name): bool
    {
        return $_SERVER['REQUEST_METHOD'] === mb_strtoupper($name);
    }

    /**
     * @return bool
     */
    public function isPost(): bool
    {
        return $this->isType('POST');
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->get->unit ?: '';
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->get->action ?: '';
    }

    public function router(): RouterInterface
    {
        return $this->router;
    }
}