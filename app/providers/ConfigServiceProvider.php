<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/3/20
 * Hora: 10:13 AM
 */

namespace eKuSchoolIn\providers;


class ConfigServiceProvider
{
    public static function get($path = null)
    {
        if ($path) {
            $path = explode('.', $path);
            $file = __DIR__ . '/../../config/' . $path[0] . '.php';
            unset($path[0]);

            if (file_exists($file)) {
                $config = include $file;
            } else {
                return false;
            }

            foreach ($path as $bit) {
                if (isset($config[$bit])) {
                    $config = $config[$bit];
                }
            }

            return $config;
        }

        return false;
    }
}