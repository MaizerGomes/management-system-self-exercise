<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 10:06 AM
 */

namespace eKuSchoolIn\providers;


use eKuSchoolIn\interfaces\ControllerResponseInterface;

/**
 * Class ResponseServiceProvider
 *
 * @package eKuSchoolIn\providers
 */
class ResponseServiceProvider
{
    /**
     * @var \eKuSchoolIn\interfaces\ControllerResponseInterface
     */
    private $response;

    /**
     * ResponseServiceProvider constructor.
     *
     * @param \eKuSchoolIn\interfaces\ControllerResponseInterface $response
     */
    public function __construct(ControllerResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     *
     */
    public function send()
    {
        return $this->response->render();
    }
}