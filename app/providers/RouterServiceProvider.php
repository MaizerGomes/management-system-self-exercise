<?php
/**
 * Criado por Maizer Aly de O. Gomes para management.
 * Email: maizer.gomes@gmail.com / maizer.gomes@ekutivasolutions / maizer.gomes@outlook.com
 * Usuário: maizerg
 * Data: 3/4/20
 * Hora: 12:21 PM
 */

namespace eKuSchoolIn\providers;


use eKuSchoolIn\interfaces\RequestServiceProviderInterface;
use eKuSchoolIn\interfaces\RouterInterface;

class RouterServiceProvider implements RouterInterface
{
    const DEFAULT_CONTROLLER   = '\\Dashboard';
    const DEFAULT_METHOD       = 'index';
    const CONTROLLER_NAMESPACE = "\\eKuSchoolIn\\controllers";

    /**
     * @var \eKuSchoolIn\controllers\BaseController
     */
    protected $controller;


    /**
     * @var \eKuSchoolIn\interfaces\RequestServiceProviderInterface
     */
    protected $requestServiceProvider;

    public function __construct(RequestServiceProviderInterface $requestServiceProvider)
    {
        $this->requestServiceProvider = $requestServiceProvider;
    }

    public function parseAction(): array
    {
        $controllerName = $this->getControllerFQDN();

        $this->controller = new $controllerName;

        try {
            return [$this->controller, $this->getMethodName()];
        } catch (\HttpRequestMethodException $e) {
            //TODO: Send Error response
            return [];
        }
    }

    public function getControllerFQDN(): string
    {
        if ($this->controller) {
            return get_class($this->controller);
        }

        $parts = explode('.', $this->requestServiceProvider->getController());
        $name = '';

        if (count($parts) > 1) {
            foreach ($parts as $part) {
                $name .= '\\' . $part;
            }
        } else {
            $name = static::DEFAULT_CONTROLLER;
        }

        return static::CONTROLLER_NAMESPACE . $name . 'Controller';
    }

    /**
     * @return string
     * @throws \HttpRequestMethodException
     */
    public function getMethodName(): string
    {
        $method = $this->requestServiceProvider->getMethod() ?: static::DEFAULT_METHOD;

        if (!method_exists($this->controller, $method)) {
            throw new \HttpRequestMethodException();
        }

        return $method;
    }

    public function getId()
    {
        // TODO: Implement getId() method.
    }
}